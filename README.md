# README #

**##### Robo BAS ics #####** 

![RB Logo.jpg](https://bitbucket.org/repo/9yGMyB/images/2166640969-RB%20Logo.jpg)

This repo contains miscellaneous Python scripts.
 
Not directly related to robots, Raspberry Pi or Arduino. 

But fun stuff made while learning

Currently only one finished project: using variable data in a PyQt4 window.

The window shows live data of the International Space Station

More info on: http://www.instructables.com/id/Dynamic-Values-in-PyQt-Window-tracking-ISS/