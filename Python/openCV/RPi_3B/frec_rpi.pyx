from imutils.video import VideoStream
#from imutils import face_utils
from imutils.video import FPS
import face_recognition
import imutils
#import numpy as np
import pickle
import time
import cv2

print("[INFO] loading encodings and face detector ...")
data = pickle.loads(open('encodings.pickle', "rb").read())
detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

print("[INFO] starting video stream...")
#vs = VideoStream(src=0).start()
vs = VideoStream(usePiCamera=True).start()
time.sleep(2.0)

totalFrames = 0

while True:
    frame = vs.read()
    H = frame.shape[0]
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = imutils.resize(gray, width=400)

    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    rgb = imutils.resize(rgb, width=400)

    r = frame.shape[1] / float(rgb.shape[1])

    if totalFrames % 30 == 0:
        trackers = cv2.MultiTracker_create()

        #boxesD = face_recognition.face_locations(rgb, model="hog")
        rects = detector.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5, minSize=(30, 30))
        boxesD = [(y, x + w, y + h, x) for (x, y, w, h) in rects]
        
        encodings = face_recognition.face_encodings(rgb, boxesD)
        names = []

        for encoding in encodings:
            matches = face_recognition.compare_faces(data["encodings"], encoding)
            name = "Onbekende"
            if True in matches:
                matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                counts = {}
                for i in matchedIdxs:
                    name = data["names"][i]
                    counts[name] = counts.get(name, 0) + 1
                name = max(counts, key=counts.get)
            names.append(name)

        for ((top, right, bottom, left), name) in zip(boxesD, names):
            boundingBox = (left, top, right - left, bottom - top)
            tracker = cv2.TrackerKCF_create()
            trackers.add(tracker, frame, boundingBox)

            top = int(top * r)
            right = int(right * r)
            bottom = int(bottom * r)
            left = int(left * r)

            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
            cv2.rectangle(frame, (left, top), (right, top - 20), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(frame, name, (left + 3, top - 3), font, 0.6, (255, 255, 255), 2)

        fps = FPS().start()
    else:
        (success, boxesT) = trackers.update(frame)
        if success:
            for ((left, top, width, height), name) in zip(boxesT, names):
                right = int((left + width) * r)
                bottom = int((top + height) * r)
                top = int(top * r)
                left = int(left * r)
                
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                cv2.rectangle(frame, (left, top), (right, top - 20), (0, 0, 255), cv2.FILLED)
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(frame, name, (left + 3, top - 3), font, 0.6, (255, 255, 255), 2)
                
        fps.update()
        fps.stop()

        info = [
            ("FPS       ", "{:.2f}".format(fps.fps())),
            ]

        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv2.putText(frame, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
        
    cv2.imshow("HAAR detector + face-recognition + KCF tracker", frame)
    
    totalFrames += 1

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
vs.stop()
