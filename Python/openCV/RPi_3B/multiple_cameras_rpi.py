from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import imutils
import time
import datetime
import cv2

webcam = VideoStream(src=-1).start()
picam  = VideoStream(usePiCamera=True).start()
time.sleep(2.0)
fps = FPS().start()

if __name__ == '__main__':
     while True:
        timestamp = datetime.datetime.now()
        ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")

        frameW = webcam.read()
        frameP = picam.read()
        frameW = imutils.resize(frameW, width=400)
        frameP = imutils.resize(frameP, width=400)
        cv2.putText(frameW, ts, (10, frameW.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (255, 255, 255), 1)
        cv2.putText(frameP, ts, (10, frameP.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (255, 255, 255), 1)
        
        cv2.imshow('webcam',frameW)
        cv2.imshow('picam',frameP)
        
        key =cv2.waitKey(1) & 0xFF 
        if key == 27:
            break

        fps.update()

fps.stop()
version = cv2.__version__
print("[INFO] CV2 version :"), (cv2.__version__)
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS : {:.2f}".format(fps.fps()))

cv2.destroyAllWindows()
webcam.stop()
picam.stop()
