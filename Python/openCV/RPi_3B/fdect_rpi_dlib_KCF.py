from imutils.video import VideoStream
from imutils import face_utils
from imutils.video import FPS
import imutils
import numpy as np
import time
import dlib
import cv2

totalFrames = 0

detector = dlib.get_frontal_face_detector()

#vs = VideoStream(src=0).start()
vs = VideoStream(usePiCamera=True).start()
time.sleep(2.0)

while True:
    frame = vs.read()
    (H,W) = frame.shape[:2]

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = imutils.resize(gray, width=400)
    (Hg, Wg) = gray.shape[:2]
    r = W/Wg
        
    if totalFrames % 30 == 0:
        trackers = cv2.MultiTracker_create()
        rects = detector(gray, 1)
    
        #print("Number of faces detected: {}".format(len(rects)))
        detected = len(rects)
        for i, d in enumerate(rects):
            #print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
            #    i, d.left(), d.top(), d.right(), d.bottom()))
            bBox = (d.left(), d.top(), d.right() - d.left(), d.bottom() - d.top())
            tracker = cv2.TrackerKCF_create()
            trackers.add(tracker, frame, bBox)
            
            l = int(bBox[0] * r)
            t = int(bBox[1] * r)
            r = int(d.right() * r)
            b = int(d.bottom() * r)
            cv2.rectangle(frame, (l, t), (r, b), (0, 255, 0), 2)
                
        fps = FPS().start()
    else:
        (success, boxes) = trackers.update(frame)
        if success:
            for box in boxes:
                (x, y, w, h) = [int(v) for v in box]
                x = int(x*r)
                y = int(y*r)
                w = int(w*r)
                h = int(h*r)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                
        fps.update()
        fps.stop()

        info = [
            ("FPS       ", "{:.2f}".format(fps.fps())),
            ]

        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv2.putText(frame, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
        
    cv2.imshow("dlib detector (resized) + KCF tracker", frame)
    
    totalFrames += 1

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
vs.stop()
