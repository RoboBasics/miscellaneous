from imutils.video import VideoStream
from imutils.video import FPS
import imutils
import numpy as np
import time
import cv2

totalFrames = 0

net = cv2.dnn.readNetFromCaffe('deploy.prototxt.txt', 'res10_300x300_ssd_iter_140000.caffemodel')

vs = VideoStream(src=0).start()
time.sleep(2.0)

while True:
    frame = vs.read()
    (H, W) = frame.shape[:2]
    frame_copy = frame
    frame_copy = imutils.resize(frame_copy, width=400)
    (Hc, Wc) = frame_copy.shape[:2]
    r = W/Wc
    
    if totalFrames % 30 == 0:
        trackers = cv2.MultiTracker_create()
        blob = cv2.dnn.blobFromImage(frame_copy, 1.0, (Wc, Hc), (104.0, 177.0, 123.0))
        
        net.setInput(blob)
        detections = net.forward()
        detected = 0
        
        for i in range(0, detections.shape[2]):
            if detections[0, 0, i, 2] > 0.5:
                rect = detections[0, 0, i, 3:7] * np.array([Wc, Hc, Wc, Hc])

                (Left, Top, Right, Bottom) = rect.astype("int")
                bBox = (Left, Top, Right - Left, Bottom - Top)
                
                confidence = detections[0, 0, i, 2]
                detected += 1

                tracker = cv2.TrackerKCF_create()
                trackers.add(tracker, frame, bBox)

                l = int(Left * r)
                t = int(Top * r)
                r = int(Right * r)
                b = int(Bottom * r)

                cv2.rectangle(frame, (l, t), (r, b), (0, 255, 0), 2)
                
                
            fps = FPS().start()
    else:
        (success, boxes) = trackers.update(frame)
        if success:
            for box in boxes:
                (x, y, w, h) = [int(v) for v in box]
                x = int(x*r)
                y = int(y*r)
                w = int(w*r)
                h = int(h*r)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                
        fps.update()
        fps.stop()

        info = [
            ("FPS       ", "{:.2f}".format(fps.fps())),
            ("Confidence", "{:.2f}%".format(confidence * 100)),
            ]

        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv2.putText(frame, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
        
    cv2.imshow("DNN detection (resized) + KCF tracker", frame)
    
    totalFrames += 1

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
vs.stop()
