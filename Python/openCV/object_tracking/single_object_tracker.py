from imutils.video import VideoStream
import centroidtracker
import numpy as np
import imutils
import time
import cv2

# initialize our centroid tracker and frame dimensions
ct = centroidtracker.CentroidTracker(maxDisappeared=40, maxDistance=30)
(H, W) = (None, None)

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe('deploy.prototxt', 'res10_300x300_ssd_iter_140000.caffemodel')

# initialize the video stream and allow the camera sensor to warmup
print("[INFO] starting video stream...")
vs = VideoStream(src=0).start()
time.sleep(2.0)

while True:
    frame = vs.read()
    
    frame_copy = cv2.resize(frame, (0, 0), fx = 1.2, fy = 1.2)
    frame = imutils.resize(frame, width=400)
    r = frame_copy.shape[0]/float (frame.shape[0])

    # if the frame dimensions are None, grab them
    if W is None or H is None:
        (H, W) = frame.shape[:2]

    # construct a blob from the frame, pass it through the network,
    # obtain our output predictions, and initialize the list of
    # bounding box rectangles
    blob = cv2.dnn.blobFromImage(frame, 1.0, (W, H),
        (104.0, 177.0, 123.0))
    net.setInput(blob)
    detections = net.forward()
    rects = []

    
    # loop over the detections
    for i in range(0, detections.shape[2]):
        # filter out weak detections by ensuring the predicted
        # probability is greater than a minimum threshold
        if detections[0, 0, i, 2] > 0.5:
            # compute the (x, y)-coordinates of the bounding box for
            # the object, then update the bounding box rectangles list
            box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
            rects.append(box.astype("int"))

            # draw a bounding box surrounding the object so we can
            # visualize it
            (startX, startY, endX, endY) = box.astype("int")
            startX = int(startX * r)
            startY = int(startY * r)
            endX = int(endX * r)
            endY = int(endY * r)
            
            cv2.rectangle(frame_copy, (startX, startY), (endX, endY),
                          (0, 255, 0), 2)

    # update our centroid tracker using the computed set of bounding
    # box rectangles
    objects = ct.update(rects)

    # loop over the tracked objects
    for (objectID, centroid) in objects.items():
        # draw both the ID of the object and the centroid of the
        # object on the output frame

        cX = int(centroid[0] * r)
        cY = int(centroid[1] * r)

        print("Adjust ",cX, cY)

        text = "ID {}".format(objectID)
        print(text)
        cv2.putText(frame_copy, text, (cX - 10, cY - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.circle(frame_copy, (cX, cY), 4, (0, 255, 0), -1)

    # show the output frame
    cv2.imshow("Frame", frame_copy)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
