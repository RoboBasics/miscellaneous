import centroidtracker
import trackableobject
from imutils.video import VideoStream
from imutils.video import FPS
import face_recognition
import imutils
import pickle
import numpy as np
import time
import dlib
import cv2

print("[INFO] loading encodings and face detector ...")
data = pickle.loads(open('encodings.pickle', "rb").read())
detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

print("[INFO] starting video stream...")
vs = VideoStream(src=0).start()
#vs = VideoStream(usePiCamera=True).start()
time.sleep(2.0)

# instantiate centroid tracker, then initialize a list to store
# each of our dlib correlation trackers, followed by a dictionary to
# map each unique object ID to a TrackableObject
ct = centroidtracker.CentroidTracker(maxDisappeared=40, maxDistance=50)
trackers = []
trackableObjects = {}

totalFrames = 0

fps = FPS().start()

while True:
    frame = vs.read()

    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #gray = imutils.resize(gray, width=300)

    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    rgb = imutils.resize(rgb, width=200)

    frame = cv2.resize(frame, (0, 0), fx = 2, fy = 2)
    r = frame.shape[1] / float(rgb.shape[1])
    H = frame.shape[0]

    # initialize current status, list of bounding box rectangles
    # returned by either (1) face detector or (2) correlation trackers
    status = "Waiting"
    rects = []

    if totalFrames % 30 == 0:
        status = "Detecting"
        trackers = []

        boxes = face_recognition.face_locations(rgb, model="hog")
        encodings = face_recognition.face_encodings(rgb, boxes)

        #rects = detector.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5, minSize=(30, 30))
        #boxes = [(y, x + w, y + h, x) for (x, y, w, h) in rects]

        encodings = face_recognition.face_encodings(rgb, boxes)
        names = []

        for encoding in encodings:
            matches = face_recognition.compare_faces(data["encodings"], encoding)
            name = "Onbekende"
            if True in matches:
                matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                counts = {}
                for i in matchedIdxs:
                    name = data["names"][i]
                    counts[name] = counts.get(name, 0) + 1
                name = max(counts, key=counts.get)
            names.append(name)

        for ((top, right, bottom, left), name) in zip(boxes, names):

            # construct a dlib rectangle object from the bounding
            # box coordinates and then start the dlib correlation
            # tracker
            tracker = dlib.correlation_tracker()
            rect = dlib.rectangle(left, top, right, bottom)
            tracker.start_track(rgb, rect)

            # add the tracker to our list of trackers so we can
            # utilize it during skip frames
            trackers.append(tracker)
    else:
        # loop over the trackers
        for tracker in trackers:
            status = "Tracking"

            # update the tracker and grab the updated position
            tracker.update(rgb)
            pos = tracker.get_position()

            # unpack the position object
            startX = int(pos.left())
            startY = int(pos.top())
            endX = int(pos.right())
            endY = int(pos.bottom())

            # add the bounding box coordinates to the rectangles list
            rects.append((startX, startY, endX, endY))

            for ((top, right, bottom, left), name) in zip(rects, names):
                top = int(startY * r)
                right = int(endX * r)
                bottom = int(endY * r)
                left = int(startX * r)

            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
            cv2.rectangle(frame, (left, top - 25), (right, top), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(frame, name, (left + 5, top - 5), font, 0.75, (255, 255, 255), 2)

    # Use centroid tracker to check old object centroids with the newly computed ones
    objects = ct.update(rects)

    # loop over the tracked objects
    for (objectID, centroid) in objects.items():
        # check to see if a trackable object exists for the current object ID
        to = trackableObjects.get(objectID, None),None

        # if there is no existing trackable object, create one
        if to is None:
            to = TrackableObject(objectID, centroid, name)

        # store the trackable object in our dictionary
        trackableObjects[objectID] = to

    # construct a tuple of information we will be displaying on the
    # frame
        info = [
        ("Frames  ", totalFrames),
        ("Detected", len(trackableObjects)),
        ("Status  ", status),
    ]
        for (i,(k,v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv2.putText(frame, text,(10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)

    cv2.imshow("FaceID", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # increment the total number of frames processed thus far and
    # then update the FPS counter
    totalFrames += 1
    fps.update()

# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
