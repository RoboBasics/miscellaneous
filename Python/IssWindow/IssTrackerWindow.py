#######################################################################################
# Window with dynamic updates
# Collects local and ISS data and displays the data in a window
#
# Coded in Python 2.7 and PyQT4
#
# Uses QThread:
#    * thread 1 = UiMainWindow class
#               => main loop
#    * thread 2 = trigger class
#               => executes variable update and generates Qt signal for main loop
#
# Handles the following data
#
#         static data              | dynamic data    
#         -------------------------|-----------------------
# Local   Local coordinates        | date & time
#         Formatted address        | local sunset & sunlight
#         Country names dictionary | Next pass Iss + duration
# ---------------------------------------------------------
# Iss     Crew members             | Check time
#                                  | Coordinates
#                                  | Above (sea or countryname, country info and timezone)
#                                  | Coordinates
#                                  | Sunlight
#                                  | Daynumber
#                                  | Velocity
#                                  | Altitude
#                                  | Footprint
######################################################################################

#!/usr/bin/python
import json
import urllib2
import datetime
import time
import sys
from uuid import getnode as get_mac
from PyQt4 import QtCore, QtGui, QtWebKit
import sys

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

dt = datetime

## Personal API_KEYS can be obtained at Google/developers, gettyimages and NASA
ApiKey1 = 'API_KEY'     # Google geolocation
ApiKey2 = 'API_KEY'     # Google (reversed) geocode 
ApiKey3 = 'API_KEY'     # gettyimages country names
ApiKey4 = 'API_KEY'     # Nasa

url_MacCoordinates     = 'https://www.googleapis.com/geolocation/v1/geolocate?key=%s' %(ApiKey1)
url_IssCoordinates     = 'https://api.wheretheiss.at/v1/satellites/25544'
url_reqCountryDataBase = 'https://api.wheretheiss.at/v1/coordinates/'
url_getIssPassBase     = 'http://api.open-notify.org/iss-pass.json?'   # lat=LAT&lon=LON'
url_getCountryNames    = 'https://api.gettyimages.com/v3/countries'
url_getCrewMembers     = 'http://api.open-notify.org/astros.json'

localPosition_RECIEVED     = False
LocalSunlightData_RECIEVED = False

IssCoordinates_RECIEVED    = False
CountryData_RECIEVED       = False
FormattedAddress_RECIEVED  = False
IssNextPassData_RECIEVED   = False
CountryNames_RECIEVED      = False

errorMsg = " "

LAT = 0.0
LON = 0.0

timeOffset = 0
localSunLight = ""

startDate         = dt.datetime.today().date()
startTime         = dt.datetime.today().time()
localEclipseDt    = dt.datetime.now()
localEclipseDate  = dt.datetime.today().date()
localEclipseTime  = dt.datetime.today().time()
localSunRiseDate  = dt.datetime.today().date()
localSunRiseTime  = dt.datetime.today().time()
timeNextPass      = dt.datetime.now()
localEclipseDtStr = ""  
timeNextPassStr   = ""
localSunLight     = ""
duration = 0

countries = []

aboveSave = ""

LinkingStorage  = ""



################################################################################################
## Collect static local data 
################################################################################################

def getLocalDateTime():
    global startDate, startTime
    startDateTime = dt.datetime.now()
    startDate = startDateTime.date()
    startTime = startDateTime.time()
    startDateTimeStr = dt.datetime.strftime(startDateTime, '%d %b %Y  %H:%M:%S')
    ui.DateValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(startDateTimeStr), None))

def getTimeOffset():
    global timeOffset
    timeOffset = dt.datetime.fromtimestamp(0) - dt.datetime.utcfromtimestamp(0)
    timeOffset = str(timeOffset)
    split = timeOffset.split(":")
    timeOffset = split[0]
    timeOffset = int(timeOffset)

def getLocalPosition():
    global url_MacCoordinates, LAT, LON, localPosition_RECIEVED, errorMsg
    mac = get_mac()
    MAC = ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    ui.MacValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>" % (MAC), None))
    data = {"wifiAccessPoints": [{"macAddress": MAC,},{"macAddress": MAC,},]}
    payload = {'some': 'data'}
    headers = {'content-type': 'application/json'}
    req = urllib2.Request(url_MacCoordinates)
    req.add_header('Content-Type', 'application/json')
    try:
        response = json.load(urllib2.urlopen(req, json.dumps(payload)))
    except urllib2.URLError as e:
        localPosition_RECIEVED = False
        errorMsg = "Local coordinates not recieved"
    else:
        localPosition_RECIEVED = True
        errorMsg = "None"
        location = response['location']
        LAT = location['lat']
        LON = location['lng']
        ACC = response['accuracy']
        LAT_Str, LON_Str = formatCoordinates(LAT,LON)
        ui.LatValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(LAT_Str),None))        
        ui.LonValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(LON_Str),None))
        ui.AccuracyValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(ACC),None))
    ui.ErrorValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-style:italic; color:#ff0000;\">%s</span></p></body></html>"%(errorMsg), None))

def formatCoordinates(Lat, Lng):
    if Lat <0:
        lat = " S"
    else:
        lat = " N"
    if LON <0:
        Lng = " W"
    else:
        lng = " E"
    latitude = abs(Lat)
    longitude = abs(Lng)
    latStr = str(latitude) + lat
    lngStr = str(longitude) + lng
    return latStr,lngStr

def getFormattedAddress():
    global LAT, LON, ApiKey2
    url_formattedAddress = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&key=%s' %(LAT,LON,ApiKey2)
    try:
        dict1 = json.load(urllib2.urlopen(url_formattedAddress))
    except urllib2.URLError as e:
        FormattedAddress_RECIEVED = False
        errorMsg = "Formatted address data not recieved"
    else:
        FormattedAddress_RECIEVED = True
        errorMsg = "None"
        dict2 = dict1['results']
        dict3 = dict2[1]
        dict4 = dict3['formatted_address']
        ui.CityCountryValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>" %(dict4), None))
    ui.ErrorValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-style:italic; color:#ff0000;\">%s</span></p></body></html>"%(errorMsg), None))

def getCountryNames():
    global url_getCountryNames,ApiKey3,CountryNames_RECIEVED,errorMsg,countries
    req = urllib2.Request(url_getCountryNames)
    req.add_header('Api-Key',ApiKey3)
    try:
        response = json.load(urllib2.urlopen(req))
    except urllib2.URLError as e:
        CountryNames_RECIEVED = False
        errorMsg = "Country names not recieved"
    else:
        CountryNames_RECIEVED = True
        errorMsg = "None"
        countries = response['countries']
    ui.ErrorValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-style:italic; color:#ff0000;\">%s</span></p></body></html>"%(errorMsg), None))

def initLocalStaticData():
    global localPosition_RECIEVED
    getLocalDateTime()
    getTimeOffset()
    getLocalPosition()
    if localPosition_RECIEVED:
        getFormattedAddress()
    getCountryNames()

################################################################################################
## Collect dynamic local data 
################################################################################################

def getLocalSunlight():
    global LocalSunlightData_RECIEVED,LAT,LON,timeOffset,localEclipseDt,localEclipseDtStr,localEclipseDate,localEclipseTime,localSunRiseDate,localSunRiseTime
    urlLocalSunlight = 'http://api.sunrise-sunset.org/json?lat=%s&lng=%s&formatted=0' % (LAT,LON)
    try: 
        localSunlight = json.load(urllib2.urlopen(urlLocalSunlight))
    except urllib2.URLError as e:
        LocalSunlightData_RECIEVED = False
        errorMsg = "Local Sunlight data not recieved"
    else:
        LocalSunlightData_RECIEVED = True
        errorMsg = "None"
        sunlightDict = localSunlight['results']
        sunDown = sunlightDict['civil_twilight_end']
        splitStr = sunDown.split("+")
        sunDownDateTime = splitStr[0]
        localEclipseDt = dt.datetime.strptime(sunDownDateTime, '%Y-%m-%dT%H:%M:%S')
        localEclipseDt = localEclipseDt + dt.timedelta(hours=timeOffset)
        localEclipseDate = localEclipseDt.date()
        localEclipseTime = localEclipseDt.time()
        localEclipseDtStr = dt.datetime.strftime(localEclipseDt, '%d %b %Y  %H:%M:%S')
        sunRise = sunlightDict['sunrise']
        splitStr = sunRise.split("+")
        SunRiseDateTime = splitStr[0]
        localSunRiseDt = dt.datetime.strptime(SunRiseDateTime, '%Y-%m-%dT%H:%M:%S')
        localSunRiseDt = localSunRiseDt + dt.timedelta(hours=timeOffset)
        localSunRiseDate = localSunRiseDt.date() 
        localSunRiseTime = localSunRiseDt.time()

def getLocalSunlightScreenData():
    global localEclipseDate,localEclipseTime,localSunRiseDate,localSunRiseTime,localSunLight
    currentTime = dt.datetime.today().time()
    if currentTime < localSunRiseTime or currentTime > localEclipseTime:
        localSunLight = "eclipsed"
    else:
        localSunLight = "daylight"

def nextPassIss():
    global url_getIssPassBase, IssNextPassData_RECIEVED,LAT,LON,timeNextPass,timeNextPassStr,duration
    Lat = str(LAT)
    Lon = str(LON)
    location = str ("lat=%s&lon=%s" %(Lat,Lon))
    url_getIssPass_Cpl = url_getIssPassBase + location
    try:
        IssPassInfo = json.load(urllib2.urlopen(url_getIssPass_Cpl))
    except urllib2.URLError as e:
        IssNextPassData_RECIEVED = False
        errorMsg = "Next pass data not recieved"
    else:
        IssNextPassData_RECIEVED = True
        errorMsg = "None"        
        response = IssPassInfo['response']
        dict = response[0]
        timeVisable = dict['risetime']
        timeNextPassStr = (dt.datetime.fromtimestamp(int(timeVisable)).strftime('%d %b %Y  %H:%M:%S'))
        timeNextPass = (dt.datetime.fromtimestamp(int(timeVisable)).strptime(timeNextPassStr, '%d %b %Y  %H:%M:%S'))
        duration = dict['duration']

def initLocalDynamicData():
    global localPosition_RECIEVED,LocalSunlightData_RECIEVED,IssNextPassData_RECIEVED,localEclipseDtStr, localSunLight,RiseTime,duration,errorMsg
    if localPosition_RECIEVED:
        getLocalSunlight()
        if LocalSunlightData_RECIEVED:
            getLocalSunlightScreenData()
            ui.LocalSunsetValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(localEclipseDtStr), None))
            ui.LocalSunlightValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(localSunLight), None))
            nextPassIss()
            if IssNextPassData_RECIEVED:
                ui.NextPassValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(timeNextPassStr), None))
                ui.DurationValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(duration), None))
    ui.ErrorValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-style:italic; color:#ff0000;\">%s</span></p></body></html>"%(errorMsg), None))

################################################################################################
## Collect static ISS data 
################################################################################################
def getCrewMembers():
    global url_getCrewMembers,CrewNames_RECIEVED,errorMsg

    CrewStr = ""
    try:
        IssCrewInfo = json.load(urllib2.urlopen(url_getCrewMembers))
    except urllib2.URLError as e:
        CrewNames_RECIEVED = False
        errorMsg = "Crewnames not recieved"
    else:
        CrewNames_RECIEVED = True
        errorMsg = "None"
        number = IssCrewInfo['number']
        people = IssCrewInfo['people']
        i = 0
        while i<number:
            name = people[i]
            member = name['name']
            if i < (number-1):
                member = member + ", "
            CrewStr = CrewStr +  member 
            i += 1
    ui.CrewValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(CrewStr), None))
    ui.ErrorValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-style:italic; color:#ff0000;\">%s</span></p></body></html>"%(errorMsg), None))

################################################################################################
## Collect dynamic ISS data 
################################################################################################

def getLocationISS():
    global IssCoordinates_RECIEVED

    try:
        response = json.load(urllib2.urlopen(url_IssCoordinates))
    except urllib2.URLError as e:
        IssCoordinates_RECIEVED = False
        errorMsg = "ISS coordinates not recieved"
    else:
        IssCoordinates_RECIEVED = True
        errorMsg = "None"
        return response

def countryName(code):
    global countries

    l = len(countries) - 1
    for t in range (0,l):
        Dict2 =  countries[t]
        if Dict2['iso_alpha_2'] == code:
            return Dict2['name']

def processResponse(response):
    global url_reqCountryDataBase, countries

    velocity = response['velocity']
    daynumber = response['daynum']
    altitude = response['altitude']
    footprint = response['footprint']
    latitude = response['latitude']
    longitude = response['longitude']
    latitudeStr = str(latitude)
    longitudeStr = str(longitude)
    url_reqCountryData_Cpl = url_reqCountryDataBase + latitudeStr + ',' + longitudeStr     

    latStr,lngStr = formatCoordinates(latitude,longitude)
    sun =  response['visibility']
    ts = dt.datetime.strftime(dt.datetime.now(), '%H:%M:%S')

    try:
        response2 = json.load(urllib2.urlopen(url_reqCountryData_Cpl))
    except urllib2.HTTPError as e:
        country = "Sea"
        timeZone=" "
        posHtml="http://www.esa.int/Our_Activities/Human_Spaceflight/International_Space_Station/Highlights/Space_Station_scrapbook"
    except urllib2.URLError as e:
        errorMsg = "ESA response not recieved"
    else:
        errorMsg = "None"
        countryCode = response2['country_code']
        country = countryName(countryCode)
        countryCodeHtml = countryCode+".html"
        posHtml = "http://www.geognos.com/api/en/countries/info/"+countryCodeHtml
        timeZone = response2['timezone_id']
    #lsDynamicIssData = str("%s|%s|%s|%s|%s|%s|%s|" % (ts,latStr,lngStr,sun,country,timeZone,posHtml ))
    lsDynamicIssData = str("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|" % (ts,latStr,lngStr,sun,country,timeZone,posHtml,daynumber,velocity,altitude,footprint ))
    return lsDynamicIssData

def getLinkingStorage():
    global IssCoordinates_RECIEVED,startDate,timeNextPass,LinkingStorage
    
    data = getLocationISS()
    if IssCoordinates_RECIEVED:
        lsData1 = processResponse(data)
    else:
        lsData1 = "--|--|--|--|--|--|--|--|--|--|--|"
    currentDateTime = dt.datetime.now()
    currentDate = currentDateTime.date()
    if currentDate != startDate:
        getLocalSunlight()
        startDate = currentDate
    getLocalSunlightScreenData()
    if currentDateTime > timeNextPass:
        nextPassIss()
    tsUpd = dt.datetime.strftime(dt.datetime.now(), '%d %b %Y  %H:%M:%S')
    lsData2 = str("%s|%s|%s|%s|%s|" %(tsUpd,localEclipseDtStr,localSunLight,timeNextPassStr,duration))
    LinkingStorage = lsData1 + lsData2 + errorMsg
    return LinkingStorage
#############################################################
class trigger(QtCore.QThread):
    threadSignal = QtCore.pyqtSignal(str)
    def __init__(self,parent=None):
        super(trigger,self).__init__(parent)
        self.exiting = False

    def run(self):
        while True:
            self.msleep(3000)
            self.threadSignal.emit(getLinkingStorage())

#############################################################
## From here: class Ui_MainWindow, consisting of:
## - self.setupUpdateThread()
## - def retranslateUi(self, MainWindow)
## - def updateWindow(self,text)
## - def setupUpdateThread(self)
#############################################################

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(2471, 1189)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setBaseSize(QtCore.QSize(0, -1))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("18thCentury"))
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("RB Logo.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout_33 = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout_33.setObjectName(_fromUtf8("gridLayout_33"))
        self.gridLayout_20 = QtGui.QGridLayout()
        self.gridLayout_20.setObjectName(_fromUtf8("gridLayout_20"))
        self.gridLayout_13 = QtGui.QGridLayout()
        self.gridLayout_13.setObjectName(_fromUtf8("gridLayout_13"))
        self.gridLayout_12 = QtGui.QGridLayout()
        self.gridLayout_12.setObjectName(_fromUtf8("gridLayout_12"))
        self.gridLayout_11 = QtGui.QGridLayout()
        self.gridLayout_11.setObjectName(_fromUtf8("gridLayout_11"))
        self.CityCountryValue = QtGui.QLabel(self.centralwidget)
        self.CityCountryValue.setMinimumSize(QtCore.QSize(0, 40))
        self.CityCountryValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.CityCountryValue.setFont(font)
        self.CityCountryValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.CityCountryValue.setFrameShadow(QtGui.QFrame.Raised)
        self.CityCountryValue.setObjectName(_fromUtf8("CityCountryValue"))
        self.gridLayout_11.addWidget(self.CityCountryValue, 0, 0, 1, 1)
        self.gridLayout_10 = QtGui.QGridLayout()
        self.gridLayout_10.setObjectName(_fromUtf8("gridLayout_10"))
        self.MacLabel = QtGui.QLabel(self.centralwidget)
        self.MacLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.MacLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.MacLabel.setFont(font)
        self.MacLabel.setFrameShape(QtGui.QFrame.WinPanel)
        self.MacLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.MacLabel.setLineWidth(0)
        self.MacLabel.setObjectName(_fromUtf8("MacLabel"))
        self.gridLayout_10.addWidget(self.MacLabel, 0, 0, 1, 1)
        self.MacValue = QtGui.QLabel(self.centralwidget)
        self.MacValue.setMinimumSize(QtCore.QSize(0, 40))
        self.MacValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.MacValue.setFont(font)
        self.MacValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.MacValue.setFrameShadow(QtGui.QFrame.Raised)
        self.MacValue.setObjectName(_fromUtf8("MacValue"))
        self.gridLayout_10.addWidget(self.MacValue, 0, 1, 1, 1)
        self.gridLayout_11.addLayout(self.gridLayout_10, 1, 0, 1, 1)
        self.DateValue = QtGui.QLabel(self.centralwidget)
        self.DateValue.setMinimumSize(QtCore.QSize(0, 40))
        self.DateValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.DateValue.setFont(font)
        self.DateValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.DateValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.DateValue.setLineWidth(0)
        self.DateValue.setObjectName(_fromUtf8("DateValue"))
        self.gridLayout_11.addWidget(self.DateValue, 2, 0, 1, 1)
        self.gridLayout_12.addLayout(self.gridLayout_11, 0, 0, 1, 1)
        self.gridLayout_9 = QtGui.QGridLayout()
        self.gridLayout_9.setObjectName(_fromUtf8("gridLayout_9"))
        self.gridLayout_8 = QtGui.QGridLayout()
        self.gridLayout_8.setObjectName(_fromUtf8("gridLayout_8"))
        self.gridLayout_7 = QtGui.QGridLayout()
        self.gridLayout_7.setObjectName(_fromUtf8("gridLayout_7"))
        self.LocalSunSetLabel = QtGui.QLabel(self.centralwidget)
        self.LocalSunSetLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.LocalSunSetLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LocalSunSetLabel.setFont(font)
        self.LocalSunSetLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.LocalSunSetLabel.setObjectName(_fromUtf8("LocalSunSetLabel"))
        self.gridLayout_7.addWidget(self.LocalSunSetLabel, 0, 0, 1, 1)
        self.NextPassLabel = QtGui.QLabel(self.centralwidget)
        self.NextPassLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.NextPassLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.NextPassLabel.setFont(font)
        self.NextPassLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.NextPassLabel.setObjectName(_fromUtf8("NextPassLabel"))
        self.gridLayout_7.addWidget(self.NextPassLabel, 1, 0, 1, 1)
        self.Dummy2 = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.Dummy2.setFont(font)
        self.Dummy2.setFrameShape(QtGui.QFrame.NoFrame)
        self.Dummy2.setText(_fromUtf8(""))
        self.Dummy2.setObjectName(_fromUtf8("Dummy2"))
        self.gridLayout_7.addWidget(self.Dummy2, 2, 0, 1, 1)
        self.gridLayout_8.addLayout(self.gridLayout_7, 0, 0, 1, 1)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.LocalSunsetValue = QtGui.QLabel(self.centralwidget)
        self.LocalSunsetValue.setMinimumSize(QtCore.QSize(0, 40))
        self.LocalSunsetValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LocalSunsetValue.setFont(font)
        self.LocalSunsetValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.LocalSunsetValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.LocalSunsetValue.setObjectName(_fromUtf8("LocalSunsetValue"))
        self.gridLayout.addWidget(self.LocalSunsetValue, 0, 0, 1, 1)
        self.NextPassValue = QtGui.QLabel(self.centralwidget)
        self.NextPassValue.setMinimumSize(QtCore.QSize(0, 40))
        self.NextPassValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.NextPassValue.setFont(font)
        self.NextPassValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.NextPassValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.NextPassValue.setObjectName(_fromUtf8("NextPassValue"))
        self.gridLayout.addWidget(self.NextPassValue, 1, 0, 1, 1)
        self.Dummy3 = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.Dummy3.setFont(font)
        self.Dummy3.setFrameShape(QtGui.QFrame.NoFrame)
        self.Dummy3.setFrameShadow(QtGui.QFrame.Sunken)
        self.Dummy3.setObjectName(_fromUtf8("Dummy3"))
        self.gridLayout.addWidget(self.Dummy3, 2, 0, 1, 1)
        self.gridLayout_8.addLayout(self.gridLayout, 0, 1, 1, 1)
        self.gridLayout_9.addLayout(self.gridLayout_8, 0, 0, 1, 1)
        self.gridLayout_6 = QtGui.QGridLayout()
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.gridLayout_5 = QtGui.QGridLayout()
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.gridLayout_4 = QtGui.QGridLayout()
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.LocalSunLightLabel = QtGui.QLabel(self.centralwidget)
        self.LocalSunLightLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.LocalSunLightLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LocalSunLightLabel.setFont(font)
        self.LocalSunLightLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.LocalSunLightLabel.setObjectName(_fromUtf8("LocalSunLightLabel"))
        self.gridLayout_4.addWidget(self.LocalSunLightLabel, 0, 0, 1, 1)
        self.DurationLabel = QtGui.QLabel(self.centralwidget)
        self.DurationLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.DurationLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.DurationLabel.setFont(font)
        self.DurationLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.DurationLabel.setObjectName(_fromUtf8("DurationLabel"))
        self.gridLayout_4.addWidget(self.DurationLabel, 1, 0, 1, 1)
        self.Dummy4 = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.Dummy4.setFont(font)
        self.Dummy4.setFrameShape(QtGui.QFrame.NoFrame)
        self.Dummy4.setText(_fromUtf8(""))
        self.Dummy4.setObjectName(_fromUtf8("Dummy4"))
        self.gridLayout_4.addWidget(self.Dummy4, 2, 0, 1, 1)
        self.gridLayout_5.addLayout(self.gridLayout_4, 0, 0, 1, 1)
        self.gridLayout_3 = QtGui.QGridLayout()
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.LocalSunlightValue = QtGui.QLabel(self.centralwidget)
        self.LocalSunlightValue.setMinimumSize(QtCore.QSize(0, 40))
        self.LocalSunlightValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LocalSunlightValue.setFont(font)
        self.LocalSunlightValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.LocalSunlightValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.LocalSunlightValue.setObjectName(_fromUtf8("LocalSunlightValue"))
        self.gridLayout_3.addWidget(self.LocalSunlightValue, 0, 0, 1, 1)
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.DurationValue = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.DurationValue.setFont(font)
        self.DurationValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.DurationValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.DurationValue.setObjectName(_fromUtf8("DurationValue"))
        self.gridLayout_2.addWidget(self.DurationValue, 0, 0, 1, 1)
        self.secLabel = QtGui.QLabel(self.centralwidget)
        self.secLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.secLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.secLabel.setFont(font)
        self.secLabel.setObjectName(_fromUtf8("secLabel"))
        self.gridLayout_2.addWidget(self.secLabel, 0, 1, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_2, 1, 0, 1, 1)
        self.Dummy5 = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.Dummy5.setFont(font)
        self.Dummy5.setFrameShape(QtGui.QFrame.NoFrame)
        self.Dummy5.setFrameShadow(QtGui.QFrame.Sunken)
        self.Dummy5.setObjectName(_fromUtf8("Dummy5"))
        self.gridLayout_3.addWidget(self.Dummy5, 2, 0, 1, 1)
        self.gridLayout_5.addLayout(self.gridLayout_3, 0, 1, 1, 1)
        self.gridLayout_6.addLayout(self.gridLayout_5, 0, 0, 3, 1)
        self.LatLabel = QtGui.QLabel(self.centralwidget)
        self.LatLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.LatLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LatLabel.setFont(font)
        self.LatLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.LatLabel.setObjectName(_fromUtf8("LatLabel"))
        self.gridLayout_6.addWidget(self.LatLabel, 0, 1, 1, 1)
        self.LatValue = QtGui.QLabel(self.centralwidget)
        self.LatValue.setMinimumSize(QtCore.QSize(0, 40))
        self.LatValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LatValue.setFont(font)
        self.LatValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.LatValue.setFrameShadow(QtGui.QFrame.Raised)
        self.LatValue.setObjectName(_fromUtf8("LatValue"))
        self.gridLayout_6.addWidget(self.LatValue, 0, 2, 1, 2)
        self.LonLabel = QtGui.QLabel(self.centralwidget)
        self.LonLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.LonLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LonLabel.setFont(font)
        self.LonLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.LonLabel.setObjectName(_fromUtf8("LonLabel"))
        self.gridLayout_6.addWidget(self.LonLabel, 1, 1, 1, 1)
        self.LonValue = QtGui.QLabel(self.centralwidget)
        self.LonValue.setMinimumSize(QtCore.QSize(0, 40))
        self.LonValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LonValue.setFont(font)
        self.LonValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.LonValue.setFrameShadow(QtGui.QFrame.Raised)
        self.LonValue.setObjectName(_fromUtf8("LonValue"))
        self.gridLayout_6.addWidget(self.LonValue, 1, 2, 1, 2)
        self.AccuracyLabel = QtGui.QLabel(self.centralwidget)
        self.AccuracyLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.AccuracyLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.AccuracyLabel.setFont(font)
        self.AccuracyLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.AccuracyLabel.setObjectName(_fromUtf8("AccuracyLabel"))
        self.gridLayout_6.addWidget(self.AccuracyLabel, 2, 1, 1, 1)
        self.AccuracyValue = QtGui.QLabel(self.centralwidget)
        self.AccuracyValue.setMinimumSize(QtCore.QSize(0, 40))
        self.AccuracyValue.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.AccuracyValue.setFont(font)
        self.AccuracyValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.AccuracyValue.setFrameShadow(QtGui.QFrame.Raised)
        self.AccuracyValue.setObjectName(_fromUtf8("AccuracyValue"))
        self.gridLayout_6.addWidget(self.AccuracyValue, 2, 2, 1, 1)
        self.mLabel = QtGui.QLabel(self.centralwidget)
        self.mLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.mLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.mLabel.setFont(font)
        self.mLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.mLabel.setObjectName(_fromUtf8("mLabel"))
        self.gridLayout_6.addWidget(self.mLabel, 2, 3, 1, 1)
        self.gridLayout_9.addLayout(self.gridLayout_6, 0, 1, 1, 1)
        self.gridLayout_12.addLayout(self.gridLayout_9, 0, 1, 1, 1)
        self.gridLayout_13.addLayout(self.gridLayout_12, 0, 0, 1, 1)
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout_13.addWidget(self.line, 1, 0, 1, 1)
        self.gridLayout_20.addLayout(self.gridLayout_13, 0, 0, 1, 1)
        self.gridLayout_19 = QtGui.QGridLayout()
        self.gridLayout_19.setObjectName(_fromUtf8("gridLayout_19"))
        self.gridLayout_18 = QtGui.QGridLayout()
        self.gridLayout_18.setObjectName(_fromUtf8("gridLayout_18"))
        self.gridLayout_17 = QtGui.QGridLayout()
        self.gridLayout_17.setObjectName(_fromUtf8("gridLayout_17"))
        self.EsawebView = QtWebKit.QWebView(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.EsawebView.sizePolicy().hasHeightForWidth())
        self.EsawebView.setSizePolicy(sizePolicy)
        self.EsawebView.setMinimumSize(QtCore.QSize(970, 440))
        self.EsawebView.setMaximumSize(QtCore.QSize(971, 471))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("18thCentury"))
        self.EsawebView.setFont(font)
        self.EsawebView.setUrl(QtCore.QUrl(_fromUtf8("http://wsn.spaceflight.esa.int/iss/index_portal.php")))
        self.EsawebView.setZoomFactor(1.5)
        self.EsawebView.setObjectName(_fromUtf8("EsawebView"))
        self.gridLayout_17.addWidget(self.EsawebView, 0, 0, 1, 1)
        self.gridLayout_16 = QtGui.QGridLayout()
        self.gridLayout_16.setObjectName(_fromUtf8("gridLayout_16"))
        self.gridLayout_14 = QtGui.QGridLayout()
        self.gridLayout_14.setObjectName(_fromUtf8("gridLayout_14"))
        self.LonLabel_2 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.LonLabel_2.sizePolicy().hasHeightForWidth())
        self.LonLabel_2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LonLabel_2.setFont(font)
        self.LonLabel_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.LonLabel_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.LonLabel_2.setObjectName(_fromUtf8("LonLabel_2"))
        self.gridLayout_14.addWidget(self.LonLabel_2, 2, 0, 1, 1)
        self.LocalSunLightLabel_2 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.LocalSunLightLabel_2.sizePolicy().hasHeightForWidth())
        self.LocalSunLightLabel_2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LocalSunLightLabel_2.setFont(font)
        self.LocalSunLightLabel_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.LocalSunLightLabel_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.LocalSunLightLabel_2.setObjectName(_fromUtf8("LocalSunLightLabel_2"))
        self.gridLayout_14.addWidget(self.LocalSunLightLabel_2, 3, 0, 1, 1)
        self.CheckTimeLabel = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.CheckTimeLabel.sizePolicy().hasHeightForWidth())
        self.CheckTimeLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.CheckTimeLabel.setFont(font)
        self.CheckTimeLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.CheckTimeLabel.setFrameShadow(QtGui.QFrame.Sunken)
        self.CheckTimeLabel.setObjectName(_fromUtf8("CheckTimeLabel"))
        self.gridLayout_14.addWidget(self.CheckTimeLabel, 0, 0, 1, 1)
        self.LatLabel_2 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.LatLabel_2.sizePolicy().hasHeightForWidth())
        self.LatLabel_2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.LatLabel_2.setFont(font)
        self.LatLabel_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.LatLabel_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.LatLabel_2.setObjectName(_fromUtf8("LatLabel_2"))
        self.gridLayout_14.addWidget(self.LatLabel_2, 1, 0, 1, 1)
        self.AboveLabel = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.AboveLabel.sizePolicy().hasHeightForWidth())
        self.AboveLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.AboveLabel.setFont(font)
        self.AboveLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.AboveLabel.setFrameShadow(QtGui.QFrame.Sunken)
        self.AboveLabel.setObjectName(_fromUtf8("AboveLabel"))
        self.gridLayout_14.addWidget(self.AboveLabel, 5, 0, 1, 1)
        self.timeZoneLabel = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.timeZoneLabel.sizePolicy().hasHeightForWidth())
        self.timeZoneLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.timeZoneLabel.setFont(font)
        self.timeZoneLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.timeZoneLabel.setFrameShadow(QtGui.QFrame.Sunken)
        self.timeZoneLabel.setObjectName(_fromUtf8("timeZoneLabel"))
        self.gridLayout_14.addWidget(self.timeZoneLabel, 4, 0, 1, 1)
        self.gridLayout_16.addLayout(self.gridLayout_14, 0, 0, 1, 1)
        self.gridLayout_15 = QtGui.QGridLayout()
        self.gridLayout_15.setObjectName(_fromUtf8("gridLayout_15"))
        self.IssDateValue = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.IssDateValue.sizePolicy().hasHeightForWidth())
        self.IssDateValue.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.IssDateValue.setFont(font)
        self.IssDateValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.IssDateValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.IssDateValue.setObjectName(_fromUtf8("IssDateValue"))
        self.gridLayout_15.addWidget(self.IssDateValue, 0, 0, 1, 1)
        self.IssLatValue = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.IssLatValue.sizePolicy().hasHeightForWidth())
        self.IssLatValue.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.IssLatValue.setFont(font)
        self.IssLatValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.IssLatValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.IssLatValue.setObjectName(_fromUtf8("IssLatValue"))
        self.gridLayout_15.addWidget(self.IssLatValue, 1, 0, 1, 1)
        self.IssLonValue = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.IssLonValue.sizePolicy().hasHeightForWidth())
        self.IssLonValue.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.IssLonValue.setFont(font)
        self.IssLonValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.IssLonValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.IssLonValue.setObjectName(_fromUtf8("IssLonValue"))
        self.gridLayout_15.addWidget(self.IssLonValue, 2, 0, 1, 1)
        self.IssSunlightValue_2 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.IssSunlightValue_2.sizePolicy().hasHeightForWidth())
        self.IssSunlightValue_2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.IssSunlightValue_2.setFont(font)
        self.IssSunlightValue_2.setFrameShape(QtGui.QFrame.WinPanel)
        self.IssSunlightValue_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.IssSunlightValue_2.setObjectName(_fromUtf8("IssSunlightValue_2"))
        self.gridLayout_15.addWidget(self.IssSunlightValue_2, 3, 0, 1, 1)
        self.timeZoneValue = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.timeZoneValue.sizePolicy().hasHeightForWidth())
        self.timeZoneValue.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.timeZoneValue.setFont(font)
        self.timeZoneValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.timeZoneValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.timeZoneValue.setObjectName(_fromUtf8("timeZoneValue"))
        self.gridLayout_15.addWidget(self.timeZoneValue, 4, 0, 1, 1)
        self.AboveValue = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.AboveValue.sizePolicy().hasHeightForWidth())
        self.AboveValue.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.AboveValue.setFont(font)
        self.AboveValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.AboveValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.AboveValue.setObjectName(_fromUtf8("AboveValue"))
        self.gridLayout_15.addWidget(self.AboveValue, 5, 0, 1, 1)
        self.gridLayout_16.addLayout(self.gridLayout_15, 0, 1, 1, 1)
        self.gridLayout_17.addLayout(self.gridLayout_16, 0, 1, 1, 1)
        self.gridLayout_18.addLayout(self.gridLayout_17, 0, 0, 1, 1)
        self.threadLabel = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.threadLabel.setFont(font)
        self.threadLabel.setObjectName(_fromUtf8("threadLabel"))
        self.gridLayout_18.addWidget(self.threadLabel, 1, 0, 1, 1)
        self.gridLayout_19.addLayout(self.gridLayout_18, 0, 0, 1, 1)
        self.webView_3 = QtWebKit.QWebView(self.centralwidget)
        self.webView_3.setMinimumSize(QtCore.QSize(460, 670))
        self.webView_3.setMaximumSize(QtCore.QSize(461, 671))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("18thCentury"))
        self.webView_3.setFont(font)
        self.webView_3.setUrl(QtCore.QUrl(_fromUtf8("http://m.esa.int/Our_Activities/Human_Spaceflight/International_Space_Station/Highlights/Space_Station_scrapbook")))
        self.webView_3.setZoomFactor(1.25)
        self.webView_3.setObjectName(_fromUtf8("webView_3"))
        self.gridLayout_19.addWidget(self.webView_3, 0, 1, 1, 1)
        self.gridLayout_20.addLayout(self.gridLayout_19, 1, 0, 1, 1)
        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.gridLayout_20.addWidget(self.line_2, 2, 0, 1, 1)
        self.gridLayout_33.addLayout(self.gridLayout_20, 0, 0, 1, 1)
        self.gridLayout_32 = QtGui.QGridLayout()
        self.gridLayout_32.setObjectName(_fromUtf8("gridLayout_32"))
        self.gridLayout_29 = QtGui.QGridLayout()
        self.gridLayout_29.setObjectName(_fromUtf8("gridLayout_29"))
        self.gridLayout_28 = QtGui.QGridLayout()
        self.gridLayout_28.setObjectName(_fromUtf8("gridLayout_28"))
        self.DayNumberLabel = QtGui.QLabel(self.centralwidget)
        self.DayNumberLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.DayNumberLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.DayNumberLabel.setFont(font)
        self.DayNumberLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.DayNumberLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.DayNumberLabel.setLineWidth(0)
        self.DayNumberLabel.setObjectName(_fromUtf8("DayNumberLabel"))
        self.gridLayout_28.addWidget(self.DayNumberLabel, 0, 0, 1, 1)
        self.FootprintLabel = QtGui.QLabel(self.centralwidget)
        self.FootprintLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.FootprintLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.FootprintLabel.setFont(font)
        self.FootprintLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.FootprintLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.FootprintLabel.setLineWidth(0)
        self.FootprintLabel.setObjectName(_fromUtf8("FootprintLabel"))
        self.gridLayout_28.addWidget(self.FootprintLabel, 1, 0, 1, 1)
        self.gridLayout_29.addLayout(self.gridLayout_28, 0, 0, 1, 1)
        self.gridLayout_27 = QtGui.QGridLayout()
        self.gridLayout_27.setObjectName(_fromUtf8("gridLayout_27"))
        self.DayNumberValue = QtGui.QLabel(self.centralwidget)
        self.DayNumberValue.setMinimumSize(QtCore.QSize(300, 40))
        self.DayNumberValue.setMaximumSize(QtCore.QSize(350, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.DayNumberValue.setFont(font)
        self.DayNumberValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.DayNumberValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.DayNumberValue.setLineWidth(0)
        self.DayNumberValue.setObjectName(_fromUtf8("DayNumberValue"))
        self.gridLayout_27.addWidget(self.DayNumberValue, 0, 0, 1, 1)
        self.FootPrintValue = QtGui.QLabel(self.centralwidget)
        self.FootPrintValue.setMinimumSize(QtCore.QSize(300, 40))
        self.FootPrintValue.setMaximumSize(QtCore.QSize(350, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.FootPrintValue.setFont(font)
        self.FootPrintValue.setMouseTracking(False)
        self.FootPrintValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.FootPrintValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.FootPrintValue.setLineWidth(0)
        self.FootPrintValue.setObjectName(_fromUtf8("FootPrintValue"))
        self.gridLayout_27.addWidget(self.FootPrintValue, 1, 0, 1, 1)
        self.gridLayout_29.addLayout(self.gridLayout_27, 0, 1, 1, 1)
        self.gridLayout_32.addLayout(self.gridLayout_29, 0, 0, 1, 1)
        self.gridLayout_30 = QtGui.QGridLayout()
        self.gridLayout_30.setObjectName(_fromUtf8("gridLayout_30"))
        self.DummyLabel = QtGui.QLabel(self.centralwidget)
        self.DummyLabel.setMinimumSize(QtCore.QSize(261, 40))
        self.DummyLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        self.DummyLabel.setObjectName(_fromUtf8("DummyLabel"))
        self.gridLayout_30.addWidget(self.DummyLabel, 0, 0, 1, 1)
        self.DummyLabel_2 = QtGui.QLabel(self.centralwidget)
        self.DummyLabel_2.setMinimumSize(QtCore.QSize(261, 40))
        self.DummyLabel_2.setMaximumSize(QtCore.QSize(16777215, 40))
        self.DummyLabel_2.setObjectName(_fromUtf8("DummyLabel_2"))
        self.gridLayout_30.addWidget(self.DummyLabel_2, 1, 0, 1, 1)
        self.gridLayout_32.addLayout(self.gridLayout_30, 0, 1, 1, 1)
        self.gridLayout_26 = QtGui.QGridLayout()
        self.gridLayout_26.setObjectName(_fromUtf8("gridLayout_26"))
        self.gridLayout_25 = QtGui.QGridLayout()
        self.gridLayout_25.setObjectName(_fromUtf8("gridLayout_25"))
        self.AltitudeLabel = QtGui.QLabel(self.centralwidget)
        self.AltitudeLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.AltitudeLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.AltitudeLabel.setFont(font)
        self.AltitudeLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.AltitudeLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.AltitudeLabel.setLineWidth(0)
        self.AltitudeLabel.setObjectName(_fromUtf8("AltitudeLabel"))
        self.gridLayout_25.addWidget(self.AltitudeLabel, 0, 0, 1, 1)
        self.VelocityLabel = QtGui.QLabel(self.centralwidget)
        self.VelocityLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.VelocityLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.VelocityLabel.setFont(font)
        self.VelocityLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.VelocityLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.VelocityLabel.setLineWidth(0)
        self.VelocityLabel.setObjectName(_fromUtf8("VelocityLabel"))
        self.gridLayout_25.addWidget(self.VelocityLabel, 1, 0, 1, 1)
        self.gridLayout_26.addLayout(self.gridLayout_25, 0, 0, 1, 1)
        self.gridLayout_24 = QtGui.QGridLayout()
        self.gridLayout_24.setObjectName(_fromUtf8("gridLayout_24"))
        self.AltitudeValue = QtGui.QLabel(self.centralwidget)
        self.AltitudeValue.setMinimumSize(QtCore.QSize(300, 40))
        self.AltitudeValue.setMaximumSize(QtCore.QSize(350, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.AltitudeValue.setFont(font)
        self.AltitudeValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.AltitudeValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.AltitudeValue.setLineWidth(0)
        self.AltitudeValue.setObjectName(_fromUtf8("AltitudeValue"))
        self.gridLayout_24.addWidget(self.AltitudeValue, 0, 0, 1, 1)
        self.VelocityValue = QtGui.QLabel(self.centralwidget)
        self.VelocityValue.setMinimumSize(QtCore.QSize(300, 40))
        self.VelocityValue.setMaximumSize(QtCore.QSize(350, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.VelocityValue.setFont(font)
        self.VelocityValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.VelocityValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.VelocityValue.setLineWidth(0)
        self.VelocityValue.setObjectName(_fromUtf8("VelocityValue"))
        self.gridLayout_24.addWidget(self.VelocityValue, 1, 0, 1, 1)
        self.gridLayout_26.addLayout(self.gridLayout_24, 0, 1, 1, 1)
        self.gridLayout_32.addLayout(self.gridLayout_26, 0, 2, 1, 1)
        self.gridLayout_31 = QtGui.QGridLayout()
        self.gridLayout_31.setObjectName(_fromUtf8("gridLayout_31"))
        self.DummyLabel_3 = QtGui.QLabel(self.centralwidget)
        self.DummyLabel_3.setMinimumSize(QtCore.QSize(261, 40))
        self.DummyLabel_3.setMaximumSize(QtCore.QSize(16777215, 40))
        self.DummyLabel_3.setObjectName(_fromUtf8("DummyLabel_3"))
        self.gridLayout_31.addWidget(self.DummyLabel_3, 0, 0, 1, 1)
        self.DummyLabel_4 = QtGui.QLabel(self.centralwidget)
        self.DummyLabel_4.setMinimumSize(QtCore.QSize(261, 40))
        self.DummyLabel_4.setMaximumSize(QtCore.QSize(16777215, 40))
        self.DummyLabel_4.setObjectName(_fromUtf8("DummyLabel_4"))
        self.gridLayout_31.addWidget(self.DummyLabel_4, 1, 0, 1, 1)
        self.gridLayout_32.addLayout(self.gridLayout_31, 0, 3, 1, 1)
        self.gridLayout_23 = QtGui.QGridLayout()
        self.gridLayout_23.setObjectName(_fromUtf8("gridLayout_23"))
        self.gridLayout_22 = QtGui.QGridLayout()
        self.gridLayout_22.setObjectName(_fromUtf8("gridLayout_22"))
        self.CrewLabel = QtGui.QLabel(self.centralwidget)
        self.CrewLabel.setMinimumSize(QtCore.QSize(0, 40))
        self.CrewLabel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.CrewLabel.setFont(font)
        self.CrewLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.CrewLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.CrewLabel.setLineWidth(0)
        self.CrewLabel.setObjectName(_fromUtf8("CrewLabel"))
        self.gridLayout_22.addWidget(self.CrewLabel, 0, 0, 1, 1)
        self.ErrorLAbel = QtGui.QLabel(self.centralwidget)
        self.ErrorLAbel.setMinimumSize(QtCore.QSize(0, 40))
        self.ErrorLAbel.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.ErrorLAbel.setFont(font)
        self.ErrorLAbel.setFrameShape(QtGui.QFrame.NoFrame)
        self.ErrorLAbel.setFrameShadow(QtGui.QFrame.Raised)
        self.ErrorLAbel.setLineWidth(0)
        self.ErrorLAbel.setObjectName(_fromUtf8("ErrorLAbel"))
        self.gridLayout_22.addWidget(self.ErrorLAbel, 1, 0, 1, 1)
        self.gridLayout_23.addLayout(self.gridLayout_22, 0, 0, 1, 1)
        self.gridLayout_21 = QtGui.QGridLayout()
        self.gridLayout_21.setObjectName(_fromUtf8("gridLayout_21"))
        self.CrewValue = QtGui.QLabel(self.centralwidget)
        self.CrewValue.setMinimumSize(QtCore.QSize(800, 40))
        self.CrewValue.setMaximumSize(QtCore.QSize(1000, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.CrewValue.setFont(font)
        self.CrewValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.CrewValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.CrewValue.setLineWidth(0)
        self.CrewValue.setObjectName(_fromUtf8("CrewValue"))
        self.gridLayout_21.addWidget(self.CrewValue, 0, 0, 1, 1)
        self.ErrorValue = QtGui.QLabel(self.centralwidget)
        self.ErrorValue.setMinimumSize(QtCore.QSize(800, 40))
        self.ErrorValue.setMaximumSize(QtCore.QSize(1000, 40))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(12)
        self.ErrorValue.setFont(font)
        self.ErrorValue.setFrameShape(QtGui.QFrame.WinPanel)
        self.ErrorValue.setFrameShadow(QtGui.QFrame.Sunken)
        self.ErrorValue.setLineWidth(0)
        self.ErrorValue.setObjectName(_fromUtf8("ErrorValue"))
        self.gridLayout_21.addWidget(self.ErrorValue, 1, 0, 1, 1)
        self.gridLayout_23.addLayout(self.gridLayout_21, 0, 1, 1, 1)
        self.gridLayout_32.addLayout(self.gridLayout_23, 0, 4, 1, 1)
        self.gridLayout_33.addLayout(self.gridLayout_32, 1, 0, 1, 1)
        self.line_3 = QtGui.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.gridLayout_33.addWidget(self.line_3, 2, 0, 1, 1)
        self.DayNumberLabel.raise_()
        self.DayNumberValue.raise_()
        self.FootprintLabel.raise_()
        self.FootPrintValue.raise_()
        self.CrewLabel.raise_()
        self.CrewValue.raise_()
        self.ErrorLAbel.raise_()
        self.ErrorValue.raise_()
        self.line_3.raise_()
        self.AltitudeLabel.raise_()
        self.AltitudeValue.raise_()
        self.VelocityValue.raise_()
        self.VelocityLabel.raise_()
        self.threadLabel.raise_()
        self.DummyLabel.raise_()
        self.DummyLabel_2.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.menuBar = QtGui.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 2471, 38))
        self.menuBar.setObjectName(_fromUtf8("menuBar"))
        self.menuFuture_Menu = QtGui.QMenu(self.menuBar)
        self.menuFuture_Menu.setObjectName(_fromUtf8("menuFuture_Menu"))
        MainWindow.setMenuBar(self.menuBar)
        self.toolBar = QtGui.QToolBar(MainWindow)
        self.toolBar.setObjectName(_fromUtf8("toolBar"))
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.toolBar_2 = QtGui.QToolBar(MainWindow)
        self.toolBar_2.setObjectName(_fromUtf8("toolBar_2"))
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar_2)
        self.actionCrewMembers = QtGui.QAction(MainWindow)
        self.actionCrewMembers.setCheckable(False)
        self.actionCrewMembers.setObjectName(_fromUtf8("actionCrewMembers"))
        self.action_Api_s = QtGui.QAction(MainWindow)
        self.action_Api_s.setObjectName(_fromUtf8("action_Api_s"))
        self.menuFuture_Menu.addAction(self.actionCrewMembers)
        self.menuFuture_Menu.addAction(self.action_Api_s)
        self.menuBar.addAction(self.menuFuture_Menu.menuAction())

        ###########################################################################
        ## Start processing-thread and connect output(signal)to window-thread(slot)
        self.setupTrigger()                         
        ###########################################################################

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "RB tracks ISS", None))
        self.CityCountryValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">Aaaaaaaaa, Bbbbbbbbbb</span></p></body></html>", None))
        self.MacLabel.setText(_translate("MainWindow", "Wifi host", None))
        self.MacValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">00:AA:00:AA:00:AA</span></p></body></html>", None))
        self.DateValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">Mmm 00, 0000 00:00:00</span></p></body></html>", None))
        self.LocalSunSetLabel.setText(_translate("MainWindow", "Sunset", None))
        self.NextPassLabel.setText(_translate("MainWindow", "Next pass ISS", None))
        self.LocalSunsetValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">Mmm 00,  0000 00:00:00</span></p></body></html>", None))
        self.NextPassValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">Mmm 00,  0000 00:00:00</span></p></body></html>", None))
        self.Dummy3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\"/></p></body></html>", None))
        self.LocalSunLightLabel.setText(_translate("MainWindow", "Sun", None))
        self.DurationLabel.setText(_translate("MainWindow", "Visible for", None))
        self.LocalSunlightValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">aaaaaaa</span></p></body></html>", None))
        self.DurationValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">0000</span></p></body></html>", None))
        self.secLabel.setText(_translate("MainWindow", "sec", None))
        self.Dummy5.setText(_translate("MainWindow", "<html><head/><body><p/></body></html>", None))
        self.LatLabel.setText(_translate("MainWindow", "Latitude", None))
        self.LatValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">000.0000000 C</span></p></body></html>", None))
        self.LonLabel.setText(_translate("MainWindow", "Longitude", None))
        self.LonValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">000.0000000 C</span></p></body></html>", None))
        self.AccuracyLabel.setText(_translate("MainWindow", "Accuracy", None))
        self.AccuracyValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">00000.0</span></p></body></html>", None))
        self.mLabel.setText(_translate("MainWindow", "m", None))
        self.LonLabel_2.setText(_translate("MainWindow", "Longitude", None))
        self.LocalSunLightLabel_2.setText(_translate("MainWindow", "Sun", None))
        self.CheckTimeLabel.setText(_translate("MainWindow", "Checked", None))
        self.LatLabel_2.setText(_translate("MainWindow", "Lattitude  ", None))
        self.AboveLabel.setText(_translate("MainWindow", "Above", None))
        self.timeZoneLabel.setText(_translate("MainWindow", "Timezone", None))
        self.IssDateValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">00:00:00</span></p></body></html>", None))
        self.IssLatValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">000.00000000000000 C</span></p></body></html>", None))
        self.IssLonValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">000.00000000000000 C</span></p></body></html>", None))
        self.IssSunlightValue_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">aaaaaaa</span></p></body></html>", None))
        self.timeZoneValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">Timezone</span></p></body></html>", None))
        self.AboveValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">Republic of Chine Mainland only and the rest of it</span></p></body></html>", None))
        self.threadLabel.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#55aa00;\">With compliments:  http://wsn.spaceflight.esa.int/iss/index_portal.php</span></p></body></html>", None))
        self.DayNumberLabel.setText(_translate("MainWindow", "Daynumber", None))
        self.FootprintLabel.setText(_translate("MainWindow", "Footprint", None))
        self.DayNumberValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">000000000000</span></p></body></html>", None))
        self.FootPrintValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">000000000000</span></p></body></html>", None))
        self.DummyLabel.setText(_translate("MainWindow", " ", None))
        self.DummyLabel_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:14pt;\">km2</span></p></body></html>", None))
        self.AltitudeLabel.setText(_translate("MainWindow", "Altitude", None))
        self.VelocityLabel.setText(_translate("MainWindow", "Velocity", None))
        self.AltitudeValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">000000000000</span></p></body></html>", None))
        self.VelocityValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">000000000000</span></p></body></html>", None))
        self.DummyLabel_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:14pt;\">km</span></p></body></html>", None))
        self.DummyLabel_4.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:14pt;\">km/h</span></p></body></html>", None))
        self.CrewLabel.setText(_translate("MainWindow", "Crew", None))
        self.ErrorLAbel.setText(_translate("MainWindow", "Error", None))
        self.CrewValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">Jeroen Huizer, Martijn Huizer, Bas Huizer</span></p></body></html>", None))
        self.ErrorValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-style:italic; color:#ff0000;\">None</span></p></body></html>", None))
        self.menuFuture_Menu.setTitle(_translate("MainWindow", "More ...", None))
        self.toolBar.setWindowTitle(_translate("MainWindow", "toolBar", None))
        self.toolBar_2.setWindowTitle(_translate("MainWindow", "toolBar_2", None))
        self.actionCrewMembers.setText(_translate("MainWindow", "&Crew", None))
        self.action_Api_s.setText(_translate("MainWindow", "&Api\'s", None))

    #################################################
    ## Process updated data and update window content
    #################################################
    def updateWindow(self,linkingStorage):
        global aboveSave
        data = linkingStorage.split("|")

        # Reload the dynamic fields
        self.IssDateValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">%s</span></p></body></html>"%(data[0]),None))
        self.IssLatValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">%s</span></p></body></html>"%(data[1]),None))
        self.IssLonValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">%s</span></p></body></html>"%(data[2]),None))
        self.IssSunlightValue_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">%s</span></p></body></html>"%(data[3]),None))
        self.AboveValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">%s</span></p></body></html>"%(data[4]),None))
        self.timeZoneValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#aaaa00;\">%s</span></p></body></html>"%(data[5]),None))
        self.DayNumberValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">%s</span></p></body></html>"%(data[7]),None))
        self.VelocityValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">%s</span></p></body></html>"%(data[8]),None))
        self.AltitudeValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">%s</span></p></body></html>"%(data[9]),None))
        self.FootPrintValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#aaaa00;\">%s</span></p></body></html>"%(data[10]),None))
        self.DateValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">%s</span></p></body></html>"%(data[11]),None))
        self.LocalSunsetValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(data[12]),None))
        self.LocalSunlightValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(data[13]),None))
        self.NextPassValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(data[14]),None))
        self.DurationValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#0000ff;\">%s</span></p></body></html>"%(data[15]),None))
        self.ErrorValue.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-style:italic; color:#ff0000;\">%s</span></p></body></html>"%(data[16]),None))
        # Check if country-info-window has to be updated
        if data[4] != aboveSave:
            self.webView_3.setUrl(QtCore.QUrl(_fromUtf8(data[6])))
            aboveSave = data[4]

    def setupTrigger(self):
        self.updateThread = trigger()
        self.updateThread.threadSignal.connect(self.updateWindow,QtCore.Qt.QueuedConnection)
        if not self.updateThread.isRunning():
            self.updateThread.start()

if __name__ == "__main__":

    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    
    initLocalStaticData()
    initLocalDynamicData()
    getCrewMembers()

    MainWindow.showMaximized()
    sys.exit(app.exec_())

