#include <Servo.h>

const unsigned int buzzPin = 11;

Servo pan;
Servo tilt;

void setup() 
{
	pinMode(buzzPin, OUTPUT);
	
	pan.attach(11);
	tilt.attach(6);

	pan.write(90);
	tilt.write(85);
}

void loop()
{
	nextAngle();
	//delay(1000);
}

void nextAngle()
{
  int angle = random(40, 110);
	changeAngle(pan, angle);

	angle = random(50, 85);
	changeAngle(tilt, angle);

  delay(500);
}


void changeAngle(Servo &actServo, int newAngle)
{
	int wait = random(30, 50);

	int actAngle = actServo.read();

	if (actAngle < newAngle) 
	{
		for (int i = actAngle; i < newAngle; i++) 
		{
			actServo.write(i);
			delay(wait);
		}
	}
	else 
	{
		for (int i = actAngle; i > newAngle; i--) 
		{
			actServo.write(i);
			delay(wait);
		}
	}
}

